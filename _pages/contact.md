---
permalink: /contact/
title: "A Beautiful Mess maakt mondmaskers"
layout: home
header:
  overlay_image: /assets/video/banner.gif
---
# CONTACT
Wij zijn gevestigd in de voormalige Bijlmer Bajes in Amsterdam. We zijn te bereiken via het e-mailadres: info@mondmaskerfabriek.nl.

# PERS
Voor meer informatie of pers aanvragen kunt u zich richten tot Deborah Timmerman, 06 239 139 04 of deborah@refugeecompany.com.

