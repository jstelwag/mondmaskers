---
permalink: /about/
title: "A Beautiful Mess maakt mondmaskers"
layout: home
header:
  overlay_image: /assets/video/banner.gif
---
# OVER ONS
Mensen die hier naartoe zijn gevlucht krijgen bij A Beautiful Mess de tijd om op adem te komen en zich te oriënteren op de Nederlandse samenleving en de arbeidsmarkt. In een veilige omgeving beginnen onze medewerkers met bouwen aan een nieuw leven. Ze werken in ons restaurant of in onze makerspace, leren de taal en verbeteren hun kennis en vaardigheden. Ze maken plezier en sluiten vriendschappen voor het leven. Met het werk dat we met z’n allen doen draagt A Beautiful Mess bij aan een positieve beeldvorming rond vluchtelingen in onze samenleving. 
Meer informatie over [A Beautiful Mess Restaurant en Makerspace vindt u hier](https://www.abeautifulmess.nl).

# SOCIAL ENTERPRISE
Sinds 2016 zijn wij aangesloten bij Social Enterprise NL. Het maken van winst is nooit het doel van onze activiteiten, maar een middel om het doel te bereiken. Wij ondernemen primair vanuit een maatschappelijke missie (impact first). In november 2019 zijn wij toegelaten tot de [Code Sociale Ondernemingen](https://www.codesocialeondernemingen.nl/). A Beautiful Mess is een initiatief van Refugee Company.
Meer [informatie over Refugee Company vindt u hier](https://www.refugeecompany.com).


