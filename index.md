---
title: "A Beautiful Mess maakt medische mondmaskers"
layout: home
header:
  overlay_image: /assets/video/banner.gif
  #overlay_filter: rgba(0, 0, 255, 0.15)
  #caption: "Photo credit: [**Unsplash**](https://unsplash.com)
---

# TEKORT MONDMASKERS
In Nederland is er op dit moment een groot tekort aan goede medische mondmaskers. Om niet afhankelijk te zijn van fabrieken aan de andere kant van de wereld is A Beautiful Mess begonnen met het versneld inrichten van een productiefaciliteit in Amsterdam. In een werkplaats in de voormalige Bijlmerbajes zullen mondmaskers worden gemaakt die aan alle eisen voldoen, door mensen met een vluchtelingenachtergrond.

# LOKALE PRODUCTIE
In samenwerking met het ministerie van [Volksgezondheid](https://www.rijksoverheid.nl/ministeries/ministerie-van-volksgezondheid-welzijn-en-sport), Welzijn en Sport (VWS) en [Philips Foundation](https://www.philips-foundation.com/) zijn we bezig om de juiste grondstoffen in te kopen, de certificering goed te regelen en een machine zo snel mogelijk naar Nederland te krijgen. Zo kunnen we vlot opschalen en een professionele, lokale productie neerzetten. We verwachten half april operationeel te zijn en uiteindelijk 100.000 mondmaskers per dag te kunnen gaan maken.

# WELK MONDMASKER PRODUCEREN WIJ?
We produceren in eerste instantie de drielaagse chirurgische maskers. Een lokale productielijn opstarten voor de FFP2 en FFP3 maskers vraagt om een grote investering en duurt zeker zes maanden, wat te lang is om een oplossing te bieden in de huidige crisis.

# JUISTE BESCHERMING
Met dit initiatief willen we ertoe bijdragen dat iedereen die helpt bij het bestrijden van de gevolgen van de coronacrisis de beschikking krijgt over de juiste beschermingsmiddelen. We beginnen met het leveren aan de essentiële beroepsgroepen. We leveren ook beschermingsmiddelen aan de specialisten van Philips; zij maken gebruik van onze maskers tijdens het onderhoud dat zij plegen aan medische apparatuur.

# VOOR IEDEREEN
We vinden dat vluchtelingen in kampen aan de rand van Europa, dak- en thuislozen en andere kwetsbare groepen ook recht hebben op bescherming tegen het virus. Daarom leveren wij onze mondmaskers tegen het 1-for-1 principe. Dus wie 100.000 maskers bestelt, stel er meteen ook 100.000 beschikbaar voor organisaties die werken met kwetsbare groepen. Wij zorgen dat die op de goede plek terecht komen.

# VOORUIT BESTELLEN
Wie mondmaskers nodig heeft kan zich (met vermelding van BIG inschrijvingsnummer en vanaf 10.000 stuks) bij ons aanmelden via mailadres info@mondmaskerfabriek.nl. 
